<!-- Headings -->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

<!-- Italics -->
\*Thus text\* is italics.

_This text_ is italics.

<!-- Strong -->

**Thus text** is italics.

__This text__ is italics.

<!-- strikethrough -->

~~This txt~~ is strikethrough.

<!-- Horizontal rule -->

---

___

<!-- Blockquote -->

>This is a quote.

<!-- Links -->

[Google](https://www.google.com)

[Google](https://www.google.com "Google")

<!-- Unordered List -->
* item 1
* item 2
* item 3
  * Nested Item 1
  * Nested Item 2

  <!-- Ordered List -->

1. Item 1
2. Item 2
3. Item 3

<!-- Inline Code Block -->

`<p>This is a paragraph.</p>`

<!-- Image Display -->

# Image Logo

![MarkDown Logo](s.png)

<!-- Github Markdown -->

<!-- Code Blocks -->

```bash
npm install
npm start
```
```javascript
function add (num1, nums2) {
    return num1 + num2;
}
```
```python
def add (num1, num2) {
    return num1 + num2;
}
```

<!-- Tables -->

|  Name  |  Email  |
| ------ | ------- |
| John   | abc@gmail.com |
| John   | def@gmail.com |

<!-- Task List -->


* [x] Task 1
* [x] Task 2
* [] Task 3